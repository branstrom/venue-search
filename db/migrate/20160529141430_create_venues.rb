class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.string :lat
      t.string :lng
      t.string :city

      t.timestamps null: false
    end
    add_index :venues, [:lat, :lng], unique: true
    add_index :venues, :city
  end
end
