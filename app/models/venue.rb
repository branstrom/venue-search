
class Venue < ActiveRecord::Base

  validates :name, presence: true
  validates :lat, presence: true
  validates :lng, presence: true, uniqueness: {
    scope: :lat, message: "must have unique location coordinates (latitude and longitude)"
  }
  reverse_geocoded_by :lat, :lng do |obj, results|
    if geo = results.first
      obj.city = geo.city
      obj.city = "London" if geo.formatted_address.include?('Greater London')
      obj.city = "Stockholm" if geo.formatted_address.include?('Sweden') && !!(geo.postal_code =~ /1\d\d \d\d/)
    end
  end
  after_validation :reverse_geocode

end
