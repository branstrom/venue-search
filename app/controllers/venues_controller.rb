class VenuesController < ApplicationController
  before_action :set_venue, only: [:edit, :update, :destroy]
  before_action :search?, only: :index

  # GET /venues
  # GET /venues.json
  def index
    @venues = Venue

    if @search.present?
      @venues = @venues.where("name ilike :search OR city ilike :search", search: ['%', @search, '%'].join)
    end

    @venues = @venues.order(name: :asc).all
  end

  def show
    @venue = Venue.find(params[:id])
    unless @venue
      redirect_to venues_path(search: params[:id])
    end
  end

  private

  def search?
    if params[:commit].presence.try(:downcase) == 'reset'
      redirect_to venues_path
      return false
    end

    @search = params[:search]
  end
end
