require 'test_helper'

class VenuesControllerTest < ActionController::TestCase

  def venue(name)
    Venue.find_by_name(name)
  end

  test "should get index and list all venues" do
    get :index
    assert_response :success
    assert_not_nil assigns(:venues)
    assert assigns(:venues).length > 10
  end

  test "search for nothing should list all venues" do
    get :index, search: ''
    assert_response :success
    assert_not_nil assigns(:venues)
    assert assigns(:venues).length > 10
  end

  test "search should find venue in Stockholm" do
    get :index, search: 'dash'
    assert_response :success
    assert_not_nil assigns(:venues)
    assert assigns(:venues).include?(venue('Haberdash'))
  end

  test "search should not find non-existant venue" do
    get :index, search: 'crash'
    assert_response :success
    assert_not_nil assigns(:venues)
    assert_empty assigns(:venues)
    assert_select 'span.alert', 'No venues found, try another spelling'
  end

  test "search should find venues in London" do
    get :index, search: 'london'
    assert_response :success
    assert_not_nil assigns(:venues)
    assert assigns(:venues).include?(venue('Marks & Spencer'))
    assert assigns(:venues).include?(venue('Citysounds'))
  end

  test "search should find venues in both London and Stockholm" do
    get :index, search: 'he'
    assert_response :success
    assert_not_nil assigns(:venues)
    assert assigns(:venues).include?(venue('Heather Bird Health of Knightsbridge Ltd'))
    assert assigns(:venues).include?(venue('Herr Judit'))
  end

  test "search should find venues in suburb of London" do
    get :index, search: 'london'
    assert_response :success
    assert_not_nil assigns(:venues)
    assert assigns(:venues).include?(venue('Harrow Breakers'))
  end

end
