require 'test_helper'

class VenueTest < ActiveSupport::TestCase

  test 'demands unique coordinates lat and lng' do
    existing_venue = Venue.find_by_name('Marks & Spencer')
    venue = Venue.create(
      name: 'Marks & Spencer Double',
      lat: existing_venue.lat,
      lng: existing_venue.lng
    )
    assert_not venue.persisted?
    assert_includes venue.errors.keys, :'lng'
  end

  test 'allows slightly unique coordinates' do
    existing_venue = Venue.find_by_name('Marks & Spencer')
    venue = Venue.create(
      name: 'Marks & Spencer Moved',
      lat: existing_venue.lat,
      lng: existing_venue.lng.to_f+0.0001
    )
    assert venue.persisted?
    assert_empty venue.errors
  end

  test 'identifies city as London from lat and lng' do
    venue = Venue.create(
      name: 'Beer Unlimited',
      lat: 51.517804,
      lng: -0.1255
    )
    assert venue.persisted?
    assert_empty venue.errors
    assert_equal 'London', venue.city
  end

  test 'identifies city as Stockholm' do
    venue = Venue.create(
      name: 'Svenska Hamburgerköket',
      lat: 59.3000912,
      lng: 18.0017584
    )
    assert venue.persisted?
    assert_empty venue.errors
    assert_equal 'Stockholm', venue.city
  end

  test 'identifies suburb Harrow of London and sets city to London' do
    venue = Venue.create(
      name: 'Harrow Niceness',
      lat: 51.5939,
      lng: -0.3528
    )
    assert venue.persisted?
    assert_empty venue.errors
    assert_equal 'London', venue.city
  end

  test 'identifies suburb Twickenham of London and sets city to London' do
    venue = Venue.create(
      name: 'Strawberry Hill House',
      lat: 51.4386338,
      lng: -0.3356083
    )
    assert venue.persisted?
    assert_empty venue.errors
    assert_equal 'London', venue.city
  end

  test 'identifies suburb Solna as Stockholm' do
    venue = Venue.create(
      name: 'Deli Garden',
      lat: 59.3703862,
      lng: 17.9993538
    )
    assert venue.persisted?
    assert_empty venue.errors
    assert_equal 'Stockholm', venue.city
  end

  test 'does not identify Uppsala as Stockholm' do
    venue = Venue.create(
      name: 'Tzatziki',
      lat: 59.8570593,
      lng: 17.6391793
    )
    assert venue.persisted?
    assert_empty venue.errors
    assert_equal 'Uppsala', venue.city
  end

end
